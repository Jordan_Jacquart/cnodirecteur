
package userclasses.paramreseau;

/**
 *
 * @author rsmon
 */

public class ParametresReseau {
     
    //  private static String ADRESSESERVEUR      = "172.16.212.182";
    //  private static String ADRESSESERVEUR      = "192.168.12.135";
    
    private static String ADRESSESERVEUR      = "localhost";
    
    private static String PORT                = "8080";
    private static String APPLI               = "WebAppliGC_PPES_BCE";
    private static String ENTREE_SERVICE      = "rest";
    
    public  static String RACINE_SERVICE_REST = "http://"+ADRESSESERVEUR+":"+PORT+"/"+
                                                 APPLI+"/"+ENTREE_SERVICE;
          
}