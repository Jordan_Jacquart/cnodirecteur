/**
 * Your application code goes here
 */
package userclasses;

import generated.StateMachineBase;
import com.codename1.ui.*;
import com.codename1.ui.events.*;
import com.codename1.ui.table.Table;
import com.codename1.ui.util.Resources;
import com.codename1.xml.Element;
import java.util.Vector;
import userclasses.modele.ModeleDeTableau;
import userclasses.parser.ParserArbreXML;
import userclasses.parser.RequetePourArbreXML;
import userclasses.parser.RequetePourTexteBrut;

/**
 *
 * @author Your name here
 */
public class StateMachine extends StateMachineBase {

    private RequetePourTexteBrut requeteSomme;
    private RequetePourArbreXML requeteRestClient;
    private Element reponseRacineArbreXmlClient;
    private RequetePourArbreXML requeteRestLesClients;
    private Element reponseRacineArbreXmlLesClients;
    private RequetePourArbreXML requeteRest;
    private Element reponseRacineArbreXmlLesRegions;
    private Table tableau;
    private ModeleDeTableau modeleDuTableau;
    private com.codename1.ui.Label errorMessage;

    public StateMachine(String resFile) {
        super(resFile);
        // do not modify, write code in initVars and initialize class members there,
        // the constructor might be invoked too late due to race conditions that might occur
    }

    @Override
    protected void postMain(Form f) {

        super.postMain(f);

        tableau = (com.codename1.ui.table.Table) findByName("Table", Display.getInstance().getCurrent());
        errorMessage = (com.codename1.ui.Label) findByName("ErrorMessage", Display.getInstance().getCurrent());

        errorMessage.setVisible(false);
        tableau.setVisible(false);
        tableau.setDrawBorder(true);
        tableau.setScrollableY(true);

        modeleDuTableau = new ModeleDeTableau("Code", "Nom", "Chiffre d'affaire annuel", "Nombre de clients");

        tableau.setModel(modeleDuTableau);
    }

    @Override
    protected void onMain_ValidationAction(Component c, ActionEvent event) {

        super.onMain_ValidationAction(c, event);


        com.codename1.ui.TextField LoginTF = (com.codename1.ui.TextField) findByName("LoginTF", Display.getInstance().getCurrent());
        com.codename1.ui.TextField PasswordTF = (com.codename1.ui.TextField) findByName("PasswordTF", Display.getInstance().getCurrent());
        com.codename1.ui.Tabs Tabs = (com.codename1.ui.Tabs) findByName("Tabs", Display.getInstance().getCurrent());
        
        
        

        String login = LoginTF.getText();
        String password = PasswordTF.getText();
        
        if(login == null || password == null || login.equals("") || password.equals("")){
            errorMessage.setVisible(true);
            return;
        }


        requeteRest = new RequetePourArbreXML();
        requeteRest.executer("/login/" + login + "/" + password);


        reponseRacineArbreXmlClient = requeteRest.getRacine();




        if (ParserArbreXML.getValeur(reponseRacineArbreXmlClient, "login").equalsIgnoreCase("error")) {
            errorMessage.setVisible(true);
            return;
        }
        
        errorMessage.setVisible(false);
        Tabs.setSelectedIndex(1);

        requeteRest = new RequetePourArbreXML();
            requeteRest.executer("/resumesregion/tous");


            Vector<Element> lesElementsClients = (Vector<Element>) requeteRest.getRacine().getChildrenByTagName("dtoregion");

            System.out.println(lesElementsClients);

            tableau.setVisible(true);

            // System.out.println(lesElementsClients);

            for (Element eltCli : lesElementsClients) {

                modeleDuTableau.ajouterRangee(
                        ParserArbreXML.getValeur(eltCli, "coderegion"),
                        ParserArbreXML.getValeur(eltCli, "nomregion"),
                        ParserArbreXML.getValeur(eltCli, "caannuel"),
                        ParserArbreXML.getValeur(eltCli, "nombreclients"));
            }

            tableau.setModel(modeleDuTableau);

    }

    protected void initVars(Resources res) {
    }
}
